let logo;
let helmet;
let move=0;
let lyskurve;
let flueben;
let ahone=23;
let ahtwo=17;
let ahthree=9;
let randomNumber;

function setup() {
	createCanvas(windowWidth, windowHeight);

	rectMode(CENTER);

	logo=loadImage("logo.png");
	helmet=loadImage("helmet.png");
	lyskurve=loadImage("lyskurve.png");
	flueben=loadImage("flueben.png")

	frameRate(120);
}

function draw() {

lyskurvef();
//
translate(10,0);
push();
paludan();
translate(0,135);
langeland();
translate(0,135);
nordreRinggade();
pop();

}

function lyskurvef(){
	background(24,154,218);
image(logo,280,20,220,100);

translate(0,50);
push();
fill(255,255,255);
rect(windowWidth/2,windowHeight/2+300,windowWidth,5);

image(helmet,0-300+move,windowHeight/2+60,250,250);
move=move+1;
console.log(move);
if(move>windowWidth+300){
	move=0;
}

if(move>250 && move<400){
	image(flueben,0-200+move,windowHeight/2+40, 50,50 );
}

image(lyskurve,windowWidth/2,windowHeight/2+3,70,300)
push();
strokeWeight(1);
//fill(255,0,0,100);
ellipse(windowWidth/2+20,windowHeight/2+35,20,20)
//fill(255,255,0,100);
ellipse(windowWidth/2+20,windowHeight/2+60,20,20)
//fill(0,255,0,100);
ellipse(windowWidth/2+20,windowHeight/2+85,20,20)

if (move>-2 && move<300){
fill(255,0,0);
ellipse(windowWidth/2+20,windowHeight/2+35,20,20)
}

if (move>600){
fill(255,0,0);
ellipse(windowWidth/2+20,windowHeight/2+35,20,20)
}

if(move>250 && move<300){
	fill(255,255,0);
	ellipse(windowWidth/2+20,windowHeight/2+60,20,20)
}


if(move>300 && move<600){
	fill(0,255,0);
	ellipse(windowWidth/2+20,windowHeight/2+85,20,20)

}
}





function paludan(){
	fill(255);
	strokeWeight(2);
	stroke(0);
	textSize(17)
	text("Ringgaden /  \nPaludan müllers vej",20,20);
	push();
	// noStroke();
	strokeWeight(1);
	textSize(15)
	text("Cyklister med cykelhjelm:",20,60);
	if(frameCount%100==0 === true && frameCount%30==0 === true){
	 ahone=ahone+floor(random(0,3));
}
	push();
	textSize(70);
	stroke(2);
	text(ahone,20,120);
	pop();
	pop();

	push();
	stroke(255);
	strokeWeight(5);
	line(10, 10, 10, 390);
	pop();

}

function langeland(){
	fill(255);
	strokeWeight(2);
	stroke(0);
	textSize(17)
	text("Ringgaden /  \nLangelandsgade",20,20);
	push();
	// noStroke();
	strokeWeight(1);
	textSize(15)
	text("Cyklister med cykelhjelm:",20,60);
	if(frameCount%70==0 === true && frameCount%50==0 === true){
	 ahtwo=ahtwo+floor(random(0,3));
}
	push();
	textSize(70);
	stroke(2);
	text(ahtwo,20,120);
	pop();
	pop();


}

function nordreRinggade(){
	fill(255);
	strokeWeight(2);
	stroke(0);
	textSize(17)
	text("Ringgaden /  \nNordreRinggade",20,20);
	push();
	// noStroke();
	strokeWeight(1);
	textSize(15)
	text("Cyklister med cykelhjelm:",20,60);
	if(frameCount%100==0 === true && frameCount%60==0 === true){
	 ahthree=ahthree+floor(random(0,3));
}
	push();
	textSize(70);
	stroke(2);
	text(ahthree,20,120);
	pop();
	pop();


}
